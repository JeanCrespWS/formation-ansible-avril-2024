#!/bin/bash
sudo apt -y update
sudo apt -y install python3-pip
yes | sudo -H pip3 install ansible==2.9.0.0 # 2.5.0.0 if issues with unquote
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt -y install terraform=0.14.0
ssh-keygen -t rsa -N '' -f home/ubuntu/.ssh/id_rsa
chown -R ubuntu:ubuntu /home/ubuntu/.ssh
git clone https://gitlab.com/wavestone/formation-ansible-v1.git /home/ubuntu/formation-ansible
chown -R ubuntu:ubuntu /home/ubuntu/formation-ansible
yes | sudo -H pip3 install ansible
