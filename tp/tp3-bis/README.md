# Troisième TP Ansible

## Informations importantes
Ce dossier comprend le troisième TP de cette formation Ansible (sous dossier exercice) et la correction associée<br>
Le but de ce TP est de prendre en main les notions d'inventaire, de rebond SSH et de collections ansible 

## Comment dérouler ce TP ?
La première chose à faire, qui n'est pas réellement nécessaire pour l'éxécution de ce TP mais qui permet de prendre en main la notion de dépendances externes, est de compléter le fichier collections/ansible_collections/requirements.yaml avec une ou plusieurs collections publiques, puis de les installer localement à l'aide de la commande
```
ansible-galaxy collection install -r collections/ansible_collections/requirements.yml
```

Il faut ensuite compléter les fichiers suivants de la partie exercice (si possible dans cet ordre) : <br>
- le fichier inventory/inventory.yml qui permet de décrire la vision qu'Ansible va avoir de votre infrastructure, ici le bastion et le reverse<br>
- Le fichier ssh.cfg qui va vous permettre de gérer vos options de connexion SSH et de forcer un rebond par votre Bastion<br>
- Les fichiers collections/ansible_collections/formation_ansible/collection_tp/roles/apache/tasks/main.yml et collections/ansible_collections/formation_ansible/collection_tp/roles/apache/handlers/main.yml qui vont vous permettre de décrire respectivement les tâches et handlers de votre rôle apache, le but de ce rôle étant d'installer un apache sur le reverse, de changer le fichier html exposé par défaut et de redémarer le service<br>
- Le fichier playbook1.yml qui va permettre d'appeler le rôle apache sur le reverse<br>

Une fois ces fichiers complétés, il faut éxécuter votre code ansible à l'aide de la commande suivante
```
ansible-playbook playbook1.yml -i inventory/
```
## Comment éxécuter la correction ?
Contrairement au TP 1 qui s'éxécutait en localhost, la correction ne peut pas être directement éxécutée dans ce TP
Il faudra d'abord compléter les fichiers inventory/inventory.yml avec les adresses du bastion et du reverse et le fichier ssh.cfg avec l'adresse du Bastion dans les deux blocs
Une fois ceci fait, le code peut-être éxécuté avec la commande sus-nommée
