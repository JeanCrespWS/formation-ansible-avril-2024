Before you stard, add a file `provider.tf` with the following lines : 

```
provider "aws" {
  region     = "eu-west-1"
  access_key = "YOUR ACCESS KEY"
  secret_key = "YOUR SECRET KEY"
}
```

Also, make sure you have a key pair in AWS (and the same key pair name in the scripts) 

# Known issues 
```
Error: error configuring Terraform AWS Provider: error validating provider credentials: error calling sts:GetCallerIdentity: SignatureDoesNotMatch: Signature expired: 20220113T201002Z is now earlier than 20220114T130954Z (20220114T132454Z - 15 min.)
        status code: 403, request id: 
  on provider.tf line 1, in provider "aws":
   1: provider "aws" {
```
## Solution
Update VM time : 
`$ sudo date -s "$(wget -qSO- --max-redirect=0 google.com 2>&1 | grep Date: | cut -d' ' -f5-8)Z"`

## ToDo 
Limit security group to Wavestone IP range
Talk about the security implication of deploying a VM on AWS accessible from the outside
