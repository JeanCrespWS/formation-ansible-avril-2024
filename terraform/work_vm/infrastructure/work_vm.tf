
################################################################################
# Compute Resources
################################################################################
resource "aws_instance" "work" {
  #for_each = var.liste_participants
  ami                         = "ami-08edbb0e85d6a0a07" 
  subnet_id                   = aws_subnet.devops_work_subnet.id
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  security_groups             = [aws_security_group.work_sg.id]
  key_name                    = local.key_name

  iam_instance_profile = "${aws_iam_instance_profile.test_profile.id}"
  
  user_data = file("${path.module}/../../../install/install.sh")

  tags = {
    Name = "work_vm_${var.participant_name}"
  }
}

################################################################################
# Network Resources
################################################################################

resource "aws_route_table" "devops_admin_rt" {
  vpc_id = aws_vpc.devops_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.devops_gw.id
  }

  tags = {
    Name = "devops_admin_rt"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.devops_work_subnet.id
  route_table_id = aws_route_table.devops_rt.id
}



resource "aws_subnet" "devops_work_subnet" {
  vpc_id     = aws_vpc.devops_vpc.id
  cidr_block = "10.0.0.0/24"

  tags = {
    Name = "devops_work_subnet"
  }
}

resource "aws_security_group" "work_sg" {
  name   = "work_sg"
  vpc_id = aws_vpc.devops_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
